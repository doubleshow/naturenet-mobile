package net.nature.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import net.nature.client.database.ApplicationData;
import net.nature.client.database.Note;
import net.nature.client.database.User;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.FileContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;

public class GoogleDriveSyncService implements SyncNoteService {
	public GoogleDriveSyncService(Context context){
		super();
		this.context = context;
	}
	
	String TAG = getClass().getSimpleName();

	static String get_access_token_api = "https://accounts.google.com//o/oauth2/token";
	
	// https://developers.google.com/drive/v2/reference/files/insert	
	static String upload_api = "https://www.googleapis.com/upload/drive/v2/files";

	static String refresh_token = "1/dJJ0ZW_mZqp-15v8nyCZBWCQHEZzeN6IQW0PZba9tl4";		
	static String client_id = "598685116719-a9iqkbdq967l6k7711g6rqmk37a47l5a.apps.googleusercontent.com";
	static String client_secret = "rZJKp6bJE2gYA9qnMoGxSEvo";
	static String folder_id = "0B9mU-w_CpbztTmZLMm0wUXhsQVk";


	final private Context context;

	public boolean isOnline(){
		ConnectivityManager cm =
				(ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;

	}

	private String getAccessToken() {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httpPost = new HttpPost(get_access_token_api);

		MultipartEntity entity = null;
		entity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);
		try {
			entity.addPart("refresh_token", new StringBody(refresh_token));
			entity.addPart("client_id", new StringBody(client_id));
			entity.addPart("client_secret", new StringBody(client_secret));
			entity.addPart("grant_type", new StringBody("refresh_token"));

			httpPost.setEntity(entity);
			//		httpPost.setHeader("Authorization", "Client-ID " + client_id);

			HttpResponse response = httpClient.execute(httpPost,
					localContext);
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							response.getEntity().getContent(), "UTF-8"));

			String sResponse = "";
			String line;
			while ((line = reader.readLine()) != null) {
				sResponse = sResponse + line;
			}

			Log.v("Drive","response:" + sResponse);

			JSONObject JResponse;
			JResponse = new JSONObject(sResponse);
			String accessToken = JResponse.getString("access_token");
			return accessToken;
		} catch (Exception e) {
			Log.e(e.getClass().getName(), e.getMessage(), e);
		}
		return null;
	}	

	public String upload(Note note){
		Log.v(TAG, "Uploading a new note to Google Drive: note = " + note);

		if (!isOnline()){
			Log.v(TAG, "Failed -> Not online");
			return null;
		}

		String	accessToken = getAccessToken();
		if (accessToken == null){
			Log.e(TAG, "Failed -> Unable to obtain an Access Token from Google");
			return null;
		}else {
			Log.v(TAG, "Obtained an access token from Google: " + accessToken);
		}

		GoogleCredential credential = new GoogleCredential();
		credential.setAccessToken(accessToken);
		credential.setExpiresInSeconds(3600L);

		Drive service = new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential)
		.setApplicationName("naturenetmobile")
		.build();


		try {
			// File's binary content
			java.io.File fileContent = new java.io.File(note.getPath());

			String mimeType = "";
			if (note.getType() == Note.Type.VIDEO){
				mimeType = "video/3gpp";
			}else if (note.getType() == Note.Type.IDEA){
				mimeType = "image/png";
			}else if (note.getType() == Note.Type.PHOTO){
				mimeType = "image/jpeg";
			}

			FileContent mediaContent = new FileContent(mimeType, fileContent);

			// File's metadata.
			File body = new File();
			body.setTitle(fileContent.getName());
			body.setMimeType(mimeType);
			body.setDescription(getDescription(note));

			File file = service.files().insert(body, mediaContent).execute();
			if (file != null) {	        	 
				String fileId = file.getId();				
				Log.v(TAG, "Note uploaded: " + file.getTitle() + ", id = " + file.getId());

				ParentReference newParent = new ParentReference();
				newParent.setId(folder_id);
				ParentReference parent = service.parents().insert(fileId, newParent).execute();
				if (parent != null){
					Log.v(TAG, "Note inserted into folder " + parent.getId());
					return file.getId();
				}					
			}

		} catch (UserRecoverableAuthIOException e) {
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (IOException e) {
			Log.e(TAG, e.getMessage(), e);
			return null;
		}		
		return null;
	}

	public Context getContext() {
		return context;
	}
	
	private String getDescription(Note note){
		User user = ApplicationData.getUser(context, note.getUserId());		
		String android_id = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID); 		
		JSONObject json = note.toJson();		
		try {
			json.put("androidId", android_id);
			json.put("avatarName", user.getAvatarName());
		} catch (JSONException e) {
		}
		return json.toString();		
	}

	@Override
	public boolean update(Note note) {
		if (!isOnline()){
			Log.v(TAG, "Failed -> Not online");
			return false;
		}

		String	accessToken = getAccessToken();
		if (accessToken == null){
			Log.e(TAG, "Failed -> Unable to obtain an Access Token from Google");
			return false;
		}else {
			Log.v(TAG, "Obtained an Access Token from Google: " + accessToken);
		}

		GoogleCredential credential = new GoogleCredential();
		credential.setAccessToken(accessToken);
		credential.setExpiresInSeconds(3600L);

		Drive service = new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential)
		.setApplicationName("naturenetmobile")
		.build();


		try {

			// File's metadata.
			File file = new File();
			file.setDescription(getDescription(note));			

			String fileId = note.getImgurId();
			Files.Patch patchRequest = service.files().patch(fileId, file);
			patchRequest.setFields("description");

			File updatedFile = patchRequest.execute();		      
			if (updatedFile != null) {	        	 
				Log.v(TAG, "Note updated!");
				return true;
			}

		} catch (UserRecoverableAuthIOException e) {
			Log.e(TAG, e.getMessage(), e);
			return false;
		} catch (IOException e) {
			Log.e(TAG, e.getMessage(), e);
			return false;
		}		
		return false;	
	}	

}
